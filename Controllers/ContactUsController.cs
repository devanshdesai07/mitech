﻿using BeautyBook.Common;
using HelpmatePublic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mitech.Controllers
{
    public class ContactUsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public JsonResult ContactUsSend(string Con_Name = "",string Con_Email = "",string Con_Subject = "",string Con_Message = "")
        {
            EmailHelper.SendEmail("ALERT : New Inquiry on your website DainTree Solution.", Con_Name, Con_Email, Con_Subject, Con_Message);
            return Json(200, JsonRequestBehavior.AllowGet);
        }
    }
}