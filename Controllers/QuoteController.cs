﻿using BeautyBook.Common;
using HelpmatePublic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mitech.Controllers
{
    public class QuoteController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetaQuoteSend(string quote_fullname = "", string quote_Email = "", string quote_Phone = "", string quote_Timeline = "", string quote_Budget = "", string quote_Describe = "")
        {
            EmailHelper.SendQuote("ALERT : New Quote from your website DainTree Solution.", quote_fullname, quote_Email, quote_Phone, quote_Timeline, quote_Budget, quote_Describe);
            return Json(200, JsonRequestBehavior.AllowGet);
        }

    }
}